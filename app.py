import re
import os
import psycopg2
import datetime
from datetime import date
from dotenv import load_dotenv
from variables_for_testing import allowed_characters
from db_commands import CREATE_ACCOUNTS_TABLE, INSERT_INTO_ACCOUNTS_TABLE, SELECT_FROM_ACCOUNTS_TABLE
from flask import Flask, render_template, request, redirect, url_for, session

load_dotenv()  # loads variables from .env file into environment

app = Flask(__name__)
url = os.environ.get("DATABASE_URL")  # gets variables from environment
conn = psycopg2.connect(url)


@app.route('/')
@app.route('/register', methods=['GET', 'POST'])
def register():
    msg = ''
    if request.method == 'POST' and 'date_of_birth' in request.form and 'password' \
            in request.form and 'email' in request.form and 'industry' in request.form:
        email = request.form['email']
        birthday = request.form['date_of_birth']

        dt = datetime.datetime.strptime(birthday, '%Y-%m-%d')
        years = (date.today().year - dt.year)

        password = request.form['password']
        industry = request.form['industry']
        if industry == "Other":
            other = request.form['specify_industry']
            occupation = None
        elif industry == "Unemployed" or industry == "Retired":
            other = None
            occupation = None
        else:
            other = None
            occupation = request.form['occupation']

        cur = conn.cursor()
        cur.execute(CREATE_ACCOUNTS_TABLE)
        cur = conn.cursor()
        cur.execute(SELECT_FROM_ACCOUNTS_TABLE, (email,))
        account = cur.fetchone()

        if account:
            msg = 'Account already exists!'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email) \
                or len(email.split("@")[0]) < 6 or len(email.split("@")[0]) > 320:
            msg = 'Invalid email address!'
        elif years < 18:
            msg = 'Date of Birth must be more than 18 years old'
        elif any(x not in allowed_characters for x in password) or len(password) < 8 or len(password) > 20:
            msg = 'Password should be from 8 to 20 digits and can contain special characters ! or @'
        elif occupation is not None:
            if len(occupation) < 2 or len(occupation) > 60 or not all(x.isalpha() or x.isspace() for x in occupation):
                msg = 'Occupation Value should be between 2 and 60 digits, no special characters & no numbers'
            else:
                cur.execute(INSERT_INTO_ACCOUNTS_TABLE, (email, birthday, password, industry, occupation, other))
                cur.connection.commit()
                msg = 'You have successfully registered!'
        elif other is not None:
            if len(other) < 2 or len(other) > 60 or not all(x.isalpha() or x.isspace() for x in other):
                msg = 'Other Value should be between 2 and 60 digits, no special characters & no numbers'
            else:
                cur.execute(INSERT_INTO_ACCOUNTS_TABLE, (email, birthday, password, industry, occupation, other))
                cur.connection.commit()
                msg = 'You have successfully registered!'
        elif not birthday or not password or not email or not industry:
            msg = 'Please fill out out all the fields!'
        else:
            cur.execute(INSERT_INTO_ACCOUNTS_TABLE, (email, birthday, password, industry, occupation, other))
            cur.connection.commit()
            msg = 'You have successfully registered!'
    elif request.method == 'POST':
        msg = 'Please fill out all the fields!'
    return render_template('register.html', msg=msg)


@app.post("/api/register")
def register_api():
    data = request.get_json()
    email = data["email"]
    birthday = data["birthday"]

    dt = datetime.datetime.strptime(birthday, '%Y-%m-%d')
    years = (date.today().year - dt.year)

    password = data["password"]
    industry = data["industry"]

    if industry == "Other":
        other = data["specify_industry"]
        occupation = None
    elif industry == "Unemployed" or industry == "Retired":
        other = None
        occupation = None
    else:
        other = None
        occupation = data["occupation"]

    cur = conn.cursor()
    cur.execute(CREATE_ACCOUNTS_TABLE)
    cur = conn.cursor()
    cur.execute(SELECT_FROM_ACCOUNTS_TABLE, (email,))
    account = cur.fetchone()

    if account:
        return {"message": "Account already exists!"}, 400
    elif not re.match(r'[^@]+@[^@]+\.[^@]+', email) or len(email.split("@")[0]) < 6 or len(email.split("@")[0]) > 320:
        return {"message": 'Invalid email address!'}, 400
    elif years < 18:
        return {"message": 'Date of Birth must be more than 18 years old'}, 400
    elif any(x not in allowed_characters for x in password) or len(password) < 8 or len(password) > 20:
        return {"message": 'Password should be from 8 to 20 digits and can contain special characters ! or @'}, 400
    elif occupation is not None:
        if len(occupation) < 2 or len(occupation) > 60 or not all(x.isalpha() or x.isspace() for x in occupation):
            return {"message": 'Occupation Value should be between 2 and 60 digits, no special characters & no numbers'}, 400
        else:
            cur.execute(INSERT_INTO_ACCOUNTS_TABLE, (email, birthday, password, industry, occupation, other))
            cur.connection.commit()
            return {"message": 'You have successfully registered!', "email": email, "birthday": birthday,
                    "password": password, "industry": industry, "occupation": occupation}, 201
    elif other is not None:
        if len(other) < 2 or len(other) > 60 or not all(x.isalpha() or x.isspace() for x in other):
            return {"message": 'Other Value should be between 2 and 60 digits, no special characters & no numbers'}, 400
        else:
            cur.execute(INSERT_INTO_ACCOUNTS_TABLE, (email, birthday, password, industry, occupation, other))
            cur.connection.commit()
            return {"message": 'You have successfully registered!', "email": email, "birthday": birthday,
                    "password": password, "industry": industry, "specify_industry": other}, 201
    elif not birthday or not password or not email or not industry:
        return {"message": 'Please fill out out all the fields!'}, 400
    else:
        cur.execute(INSERT_INTO_ACCOUNTS_TABLE, (email, birthday, password, industry, occupation, other))
        cur.connection.commit()
        return {"message": 'You have successfully registered!', "email": email, "birthday": birthday,
                "password": password, "industry": industry}, 201

    return {"message": "Signup Completed Successfully", "email": email, "birthday": birthday,
            "password": password, "industry": industry}, 201


if __name__ == '__main__':
    app.run()
