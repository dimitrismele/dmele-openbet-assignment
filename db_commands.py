CREATE_ACCOUNTS_TABLE = (
    "CREATE TABLE IF NOT EXISTS accounts (id SERIAL PRIMARY KEY, emailaddress varchar(320) NOT NULL, "
    "birthday DATE NOT NULL, password varchar(20) NOT NULL,  industry varchar(20)  NOT NULL, occupation varchar(60),"
    "other varchar(60));"
)

INSERT_INTO_ACCOUNTS_TABLE = ("INSERT INTO accounts (emailaddress, birthday, password, industry, occupation, other) "
                              "VALUES (%s, %s, %s,  %s,  %s,  %s) RETURNING id;")

SELECT_FROM_ACCOUNTS_TABLE = "SELECT * FROM accounts WHERE emailaddress = %s"
