# OpenBet Technical Assignment Dimitris Mele

This project was generated with Flask Application.
In order to run this project some packages need to be downloaded if they do not already exist in your workstations.<br>
<br>To make the process easier here are some commands for the creation of a virtual environment (venv) and how to install 
the packages needed


##Step 1. Install python and Pycharm Community edition
Python: https://www.python.org/downloads/ 
<br>
Pycharm IDE: https://www.jetbrains.com/pycharm/download/#section=windows


##Step 2: Configure a virtual environment
Run the following instructions:
https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html


##Step 3. Install the requirements needed manually 
###Emit this step if you created a venv as described in Step 2
Inside the project files you will find the requirements.txt file. 
Install the packages either by running the following command:
```bash
pip install -r requirements.txt
```
or click inside the requirements.txt file and a popup will suggest you to install them.
Click on and it and in the popup that will be displayed click install.
Wait until all libraries are installed and move to next step.


##Step 4. Run Flask application
When everything has been installed successfully run the app.py file

**Press the Run from the main menu**


##Step 5. Run UI Tests
As soon as the flask app is up and running go to tests>ui_tests and execute them.

**Run the ui_tests from the main menu**


## Support / Help
To get help on the project if something is not working as expected please contact Dimitris Mele.

email : **dimitris.mele@gmail.com** / tel: **6971805130**