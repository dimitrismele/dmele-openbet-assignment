function checkIfOther() {
    if (document.getElementById('industry').value == 'Other') {
        document.getElementById('extra2').style.display = 'none';
        document.getElementById('extra').style.display = '';
        document.getElementById('specify_industry').disabled = false;
    }
    else if (document.getElementById('industry').value == 'Unemployed' ||
        document.getElementById('industry').value == 'Retired') {
        document.getElementById('extra').style.display = 'none';
        document.getElementById('extra2').style.display = 'none';
    }
    else {
        document.getElementById('extra').style.display = 'none';
        document.getElementById('extra2').style.display = '';
        document.getElementById('occupation').disabled = false;

    }
}
