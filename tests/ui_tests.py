import random
import string
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from variables_for_testing import occupation_length_big


class OpenBet_SignUp(unittest.TestCase):

    # Functions written to be called in tests written below
    def random_string(self, length):
        # choose from all lowercase letter
        letters = string.ascii_lowercase
        result_str = ''.join(random.choice(letters) for i in range(length))
        new_email = 'user_'+result_str+'@gmail.com'
        return new_email

    def email(self, email):
        return self.driver.find_element(By.ID, value="email").send_keys(email)

    def birthday(self, date):
        return self.driver.find_element(By.ID, value="date_of_birth").send_keys(date)

    def password(self, password):
        return self.driver.find_element(By.NAME, value="password").send_keys(password)

    def industry(self, industry):
        select = Select(self.driver.find_element(By.ID, value='industry'))
        return select.select_by_value(industry)

    def occupation(self, occupation):
        return self.driver.find_element(By.NAME, value="occupation").send_keys(occupation)

    def specify_industry(self, specify_industry):
        return self.driver.find_element(By.NAME, value="specify_industry").send_keys(specify_industry)

    def submit_button(self):
        return self.driver.find_element(By.ID, value="submit").click()

    def get_msg(self):
        get_message = self.driver.find_element(By.NAME, value="msg").text
        return get_message

    def setUp(self):
        self.driver = webdriver.Chrome('./chromedriver')

    def tearDown(self):
        self.driver.close()

    def test_complete_successfully_signup(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Other")
        self.specify_industry('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("You have successfully registered!", message)

    def test_email_already_exists(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email("dimitrismele@gmail.com")
        self.birthday('13031991')
        self.password("123456qa!")
        self.industry("Other")
        self.specify_industry('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Account already exists!", message)

    def test_email_lower_than_min_length(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(0))
        self.birthday('13031991')
        self.password("123456qa!")
        self.industry("Other")
        self.specify_industry('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Invalid email address!", message)

    def test_email_higher_than_max_length(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(321))
        self.birthday('13031991')
        self.password("123456qa!")
        self.industry("Other")
        self.specify_industry('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Invalid email address!", message)

    def test_email_min_length(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(6))
        self.birthday('13031991')
        self.password("123456qa!")
        self.industry("Other")
        self.specify_industry('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("You have successfully registered!", message)

    # This is test is aborted by the DB Free Plan
    # def test_email_max_length(self):
    #     self.driver.get("http://127.0.0.1:5000/register")
    #     self.email(self.random_string(315))
    #     self.birthday('13031991')
    #     self.password("123456qa!")
    #     self.industry("Other")
    #     self.specify_industry('Life Sciences')
    #     self.submit_button()
    #     message = self.get_msg()
    #     self.assertIn("You have successfully registered!", message)

    def test_date_lower_than_18_years(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(6))
        self.birthday('13032020')
        self.password("123456qa!")
        self.industry("Other")
        self.specify_industry('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Date of Birth must be more than 18 years old", message)

    def test_invalid_email_address(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email("dimitrismelegmail.com")
        self.birthday('13031991')
        self.password("123456qa!")
        self.industry("Other")
        self.specify_industry('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Invalid email address!", message)

    def test_longer_password_needed(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456")
        self.industry("Other")
        self.specify_industry('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Password should be from 8 to 20 digits and can contain special characters ! or @", message)

    def test_smaller_password_needed(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456789011121314151617181920")
        self.industry("Other")
        self.specify_industry('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Password should be from 8 to 20 digits and can contain special characters ! or @", message)

    def test_password_not_acceptable_special_chars(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("12345678#$%^&*")
        self.industry("Other")
        self.specify_industry('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Password should be from 8 to 20 digits and can contain special characters ! or @", message)

    def test_password_acceptable_special_chars_and_min_length(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456!@")
        self.industry("Other")
        self.specify_industry('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("You have successfully registered!", message)

    def test_industry_retired(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Retired")
        self.submit_button()
        message = self.get_msg()
        self.assertIn("You have successfully registered!", message)

    def test_industry_unemployed(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Unemployed")
        self.submit_button()
        message = self.get_msg()
        self.assertIn("You have successfully registered!", message)

    def test_occupation_length_small(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Other")
        self.specify_industry('A')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Other Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_occupation_length_big(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Other")
        self.specify_industry(occupation_length_big)
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Other Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_occupation_contains_numbers(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Other")
        self.specify_industry('Life Sciences 56789898')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Other Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_occupation_contains_special_characters(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Other")
        self.specify_industry('Life Sciences !@#$%^')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Other Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_happy_path_industry_Agriculture_specify_industry(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Agriculture")
        self.occupation('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("You have successfully registered!", message)

    def test_industry_Agriculture_specify_industry_length_small(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Agriculture")
        self.occupation('A')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_Agriculture_specify_industry_length_big(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Agriculture")
        self.occupation(occupation_length_big)
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_Agriculture_specify_industry_numbers(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Agriculture")
        self.occupation('Life Sciences 56789898')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_Agriculture_specify_industry_special_characters(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Agriculture")
        self.occupation('Life Sciences !@#$%^')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_happy_path_industry_IT_specify_industry(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("IT")
        self.occupation('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("You have successfully registered!", message)

    def test_industry_IT_specify_industry_length_small(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("IT")
        self.occupation('A')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_IT_specify_industry_IT_length_big(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("IT")
        self.occupation(occupation_length_big)
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_IT_specify_industry_numbers(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("IT")
        self.occupation('Life Sciences 56789898')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_IT_specify_industry_special_characters(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("IT")
        self.occupation('Life Sciences !@#$%^')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_happy_path_industry_Education_specify_industry(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Education")
        self.occupation('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("You have successfully registered!", message)

    def test_industry_Education_specify_industry_length_small(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Education")
        self.occupation('A')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_Education_specify_industry_length_big(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Education")
        self.occupation(occupation_length_big)
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_Education_specify_industry_numbers(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Education")
        self.occupation('Life Sciences 56789898')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_Education_specify_industry_special_characters(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Education")
        self.occupation('Life Sciences !@#$%^')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_happy_path_industry_Healthcare_specify_industry(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Healthcare")
        self.occupation('Life Sciences')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("You have successfully registered!", message)

    def test_industry_Healthcare_specify_industry_length_small(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Healthcare")
        self.occupation('A')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_Healthcare_specify_industry_length_big(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Healthcare")
        self.occupation(occupation_length_big)
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_Healthcare_specify_industry_numbers(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Healthcare")
        self.occupation('Life Sciences 56789898')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)

    def test_industry_Healthcare_specify_industry_special_characters(self):
        self.driver.get("http://127.0.0.1:5000/register")
        self.email(self.random_string(8))
        self.birthday('13031991')
        self.password("123456qa")
        self.industry("Healthcare")
        self.occupation('Life Sciences !@#$%^')
        self.submit_button()
        message = self.get_msg()
        self.assertIn("Occupation Value should be between 2 and 60 digits, no special characters & no numbers", message)


    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
