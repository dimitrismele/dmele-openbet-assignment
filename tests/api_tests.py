import json
import string
import random
import unittest
from app import app
from variables_for_testing import occupation_length_big


class OpenBet_API_SignUp(unittest.TestCase):

    # Functions written to be called in tests written below
    def random_string(self, length):
        # choose from all lowercase letter
        letters = string.ascii_lowercase
        result_str = ''.join(random.choice(letters) for i in range(length))
        new_email = 'user_' + result_str + '@gmail.com'
        return new_email

    def setUp(self):
        self.app = app.test_client()

    def test_api_successful_signup(self):
        email = self.random_string(8)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456qa!",
            "industry": "IT",
            "occupation": "Senior QA Engineer"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual(email, response.json['email'])
        self.assertEqual("1991-12-03", response.json['birthday'])
        self.assertEqual("123456qa!", response.json['password'])
        self.assertEqual("IT", response.json['industry'])
        self.assertEqual("Senior QA Engineer", response.json['occupation'])
        self.assertEqual(str, type(response.json['email']))
        self.assertEqual(str, type(response.json['birthday']))
        self.assertEqual(str, type(response.json['password']))
        self.assertEqual(str, type(response.json['industry']))
        self.assertEqual(str, type(response.json['occupation']))
        self.assertEqual("You have successfully registered!", response.json['message'])
        self.assertEqual(201, response.status_code)

    def test_api_email_already_exists(self):
        payload = json.dumps({
            "email": "dimitrismele@gmail.com",
            "birthday": "1991-12-03",
            "password": "123456qa!",
            "industry": "IT",
            "occupation": "Senior QA Engineer"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Account already exists!", response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_email_lower_than_min_length(self):
        email = self.random_string(0)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456qa!",
            "industry": "IT",
            "occupation": "Senior QA Engineer"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Invalid email address!", response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_email_higher_than_max_length(self):
        email = self.random_string(321)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456qa!",
            "industry": "IT",
            "occupation": "Senior QA Engineer"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Invalid email address!", response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_email_min_length(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456qa!",
            "industry": "IT",
            "occupation": "Senior QA Engineer"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual(email, response.json['email'])
        self.assertEqual("1991-12-03", response.json['birthday'])
        self.assertEqual("123456qa!", response.json['password'])
        self.assertEqual("IT", response.json['industry'])
        self.assertEqual("Senior QA Engineer", response.json['occupation'])
        self.assertEqual(str, type(response.json['email']))
        self.assertEqual(str, type(response.json['birthday']))
        self.assertEqual(str, type(response.json['password']))
        self.assertEqual(str, type(response.json['industry']))
        self.assertEqual(str, type(response.json['occupation']))
        self.assertEqual("You have successfully registered!", response.json['message'])
        self.assertEqual(201, response.status_code)

    def test_api_date_lower_than_18_years(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "2020-12-03",
            "password": "123456qa!",
            "industry": "IT",
            "occupation": "Senior QA Engineer"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Date of Birth must be more than 18 years old", response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_invalid_email_address(self):
        payload = json.dumps({
            "email": "dimitrismelegmail.com",
            "birthday": "1991-12-03",
            "password": "123456qa!",
            "industry": "IT",
            "occupation": "Senior QA Engineer"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Invalid email address!", response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_longer_password_needed(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456",
            "industry": "IT",
            "occupation": "Senior QA Engineer"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Password should be from 8 to 20 digits and can contain special characters ! or @",
                         response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_smaller_password_needed(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456789011121314151617181920",
            "industry": "IT",
            "occupation": "Senior QA Engineer"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Password should be from 8 to 20 digits and can contain special characters ! or @",
                         response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_password_not_acceptable_special_chars(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "12345678#$%^&*",
            "industry": "IT",
            "occupation": "Senior QA Engineer"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Password should be from 8 to 20 digits and can contain special characters ! or @",
                         response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_password_acceptable_special_chars_and_min_length(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456!@",
            "industry": "IT",
            "occupation": "Senior QA Engineer"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual(email, response.json['email'])
        self.assertEqual("1991-12-03", response.json['birthday'])
        self.assertEqual("123456!@", response.json['password'])
        self.assertEqual("IT", response.json['industry'])
        self.assertEqual("Senior QA Engineer", response.json['occupation'])
        self.assertEqual(str, type(response.json['email']))
        self.assertEqual(str, type(response.json['birthday']))
        self.assertEqual(str, type(response.json['password']))
        self.assertEqual(str, type(response.json['industry']))
        self.assertEqual(str, type(response.json['occupation']))
        self.assertEqual("You have successfully registered!", response.json['message'])
        self.assertEqual(201, response.status_code)

    def test_api_industry_retired(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456!@",
            "industry": "Retired",
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("You have successfully registered!", response.json['message'])
        self.assertEqual(201, response.status_code)

    def test_api_industry_unemployed(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456!@",
            "industry": "Unemployed",
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual(email, response.json['email'])
        self.assertEqual("1991-12-03", response.json['birthday'])
        self.assertEqual("123456!@", response.json['password'])
        self.assertEqual("Unemployed", response.json['industry'])
        self.assertEqual(str, type(response.json['email']))
        self.assertEqual(str, type(response.json['birthday']))
        self.assertEqual(str, type(response.json['password']))
        self.assertEqual(str, type(response.json['industry']))
        self.assertEqual("You have successfully registered!", response.json['message'])
        self.assertEqual(201, response.status_code)

    def test_api_occupation_length_big(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456!@",
            "industry": "Other",
            "specify_industry": occupation_length_big
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Other Value should be between 2 and 60 digits, no special characters & no numbers",
                         response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_occupation_contains_numbers(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456!@",
            "industry": "Other",
            "specify_industry": "Life Sciences 56789898"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Other Value should be between 2 and 60 digits, no special characters & no numbers",
                         response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_happy_path_industry_Agriculture_specify_industry(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456!@",
            "industry": "Agriculture",
            "occupation": "Life Sciences"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual(email, response.json['email'])
        self.assertEqual("1991-12-03", response.json['birthday'])
        self.assertEqual("123456!@", response.json['password'])
        self.assertEqual("Agriculture", response.json['industry'])
        self.assertEqual("Life Sciences", response.json['occupation'])
        self.assertEqual(str, type(response.json['email']))
        self.assertEqual(str, type(response.json['birthday']))
        self.assertEqual(str, type(response.json['password']))
        self.assertEqual(str, type(response.json['industry']))
        self.assertEqual("You have successfully registered!", response.json['message'])
        self.assertEqual(201, response.status_code)

    def test_api_industry_Agriculture_specify_industry_length_small(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456!@",
            "industry": "Agriculture",
            "occupation": "A"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Occupation Value should be between 2 and 60 digits, no special characters & no numbers",
                         response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_industry_Agriculture_specify_industry_length_big(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456!@",
            "industry": "Agriculture",
            "occupation": occupation_length_big
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Occupation Value should be between 2 and 60 digits, no special characters & no numbers",
                         response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_api_industry_Agriculture_specify_industry_length_small(self):
        email = self.random_string(6)
        payload = json.dumps({
            "email": email,
            "birthday": "1991-12-03",
            "password": "123456!@",
            "industry": "Agriculture",
            "occupation": "Life Sciences !@#$%^"
        })
        # When
        response = self.app.post('/api/register', headers={"Content-Type": "application/json"}, data=payload)
        # Then
        self.assertEqual("Occupation Value should be between 2 and 60 digits, no special characters & no numbers",
                         response.json['message'])
        self.assertEqual(400, response.status_code)
